#![deny(missing_docs)]

/*!
This crate defines an infrastructure to handle collision detection.

It's based on the `collide` crate, so your colliders need need to implement the `Collider` trait.

The `CollisionManager`, which can only contain one type of collider.
When you want to use multiple colliders, you currently need to use trait objects or enums.

Then you can colliders to the manager. You should store their indices, which will be used when you modify or remove them.
When you're done, you can compute the collisions, which you can use to update your objects.
You can either compute the collisions between all objects internally or between this object with a different layer.
Before computing the collisions again, you should update your colliders, for example if the position of the object changed.
**/

use array_linked_list::ArrayLinkedList;
use collide::{Collider, CollisionInfo};
use vector_space::VectorSpace;

/// The collision info with the index of the other collider.
pub struct IndexedCollisionInfo<V: VectorSpace, I> {
    /// Index of the object.
    pub index: I,
    /// The actual collision info.
    pub info: CollisionInfo<V>,
}

struct Indexed<C: Collider, I> {
    index: I,
    collider: C,
}

/// The collision manager, which manages collisions.
/// Can contain multiple colliders.
pub struct CollisionManager<C: Collider, I> {
    colliders: ArrayLinkedList<Indexed<C, I>>,
}

impl<C: Collider, I: Copy> CollisionManager<C, I> {
    /// Creates a new collision manager.
    pub fn new() -> Self {
        Self {
            colliders: ArrayLinkedList::new(),
        }
    }

    /// Creates a new collision manager with the capacity of expected colliders specified.
    pub fn with_capacity(capacity: usize) -> Self {
        Self {
            colliders: ArrayLinkedList::with_capacity(capacity),
        }
    }

    /// Inserts a new collider and returns its index.
    /// An object index needs to be specified.
    pub fn insert_collider(&mut self, collider: C, index: I) -> usize {
        self.colliders.push_back(Indexed { collider, index })
    }

    /// Replaces the existing collider at the specified index by a new collider.
    pub fn replace_collider(&mut self, index: usize, collider: C) {
        self.colliders[index].as_mut().unwrap().collider = collider;
    }

    /// Removes an existing collider, so the index is usable for a new collect again.
    pub fn remove_collider(&mut self, index: usize) {
        self.colliders.remove(index);
    }

    /// Get the collider at the sepcified index.
    pub fn collider(&self, index: usize) -> &C {
        &self.colliders[index].as_ref().unwrap().collider
    }

    /// Get the collider at the sepcified index as mutable.
    pub fn collider_mut(&mut self, index: usize) -> &mut C {
        &mut self.colliders[index].as_mut().unwrap().collider
    }

    /// Checks if theer is a collision at a specific position.
    pub fn check_collision(&self, check_collider: &C) -> bool {
        for collider in &self.colliders {
            if check_collider.check_collision(&collider.collider) {
                return true;
            }
        }
        false
    }

    /// Checks for a collision with collider and returns some index if found.
    pub fn find_collision(&self, check_collider: &C) -> Option<I> {
        for collider in &self.colliders {
            if check_collider.check_collision(&collider.collider) {
                return Some(collider.index);
            }
        }
        None
    }

    /// Finds all collisions with colliders and returns their indices.
    pub fn find_collisions(&self, check_collider: &C) -> Vec<I> {
        let mut result = Vec::new();
        for collider in &self.colliders {
            if check_collider.check_collision(&collider.collider) {
                result.push(collider.index);
            }
        }
        result
    }

    /// Computes the internal collisions between all colliders.
    /// Returns a list for each, which can be indexed by the specified index.
    pub fn compute_inner_collisions(
        &self,
    ) -> ArrayLinkedList<Vec<IndexedCollisionInfo<C::Vector, I>>> {
        let mut result = ArrayLinkedList::with_capacity(self.colliders.capacity());
        for collider_index in self.colliders.indices() {
            result.replace_front(collider_index, Vec::new());
        }
        for (collider_index, indexed_collider) in self.colliders.indexed() {
            let infos = result[collider_index].as_mut().unwrap() as *mut _;
            // check dynamic collisions
            for (other_index, other_dynamic) in self.colliders.indexed_after(collider_index) {
                let dynamic_collider = &other_dynamic.collider;
                let other_infos = result[other_index].as_mut().unwrap();
                if let Some(info) = indexed_collider.collider.collision_info(dynamic_collider) {
                    other_infos.push(IndexedCollisionInfo {
                        index: indexed_collider.index,
                        info: -info,
                    });
                    unsafe {
                        std::mem::transmute::<_, &mut Vec<IndexedCollisionInfo<C::Vector, I>>>(
                            infos,
                        )
                    }
                    .push(IndexedCollisionInfo {
                        index: other_dynamic.index,
                        info,
                    });
                }
            }
        }
        result
    }

    /// Computes the collisions between all colliders with the colliders of another collision manager.
    /// Returns a list for each, which can be indexed by the specified index.
    pub fn compute_collisions_with(
        &self,
        other: &Self,
    ) -> ArrayLinkedList<Vec<IndexedCollisionInfo<C::Vector, I>>> {
        let mut result = ArrayLinkedList::with_capacity(self.colliders.capacity());
        for collider_index in self.colliders.indices() {
            result.replace_front(collider_index, Vec::new());
        }
        for (collider_index, indexed_collider) in self.colliders.indexed() {
            let infos = result[collider_index].as_mut().unwrap();
            // check static collisions
            for collider_static in &other.colliders {
                if let Some(info) = indexed_collider
                    .collider
                    .collision_info(&collider_static.collider)
                {
                    infos.push(IndexedCollisionInfo {
                        index: collider_static.index,
                        info,
                    });
                }
            }
        }
        result
    }
}
